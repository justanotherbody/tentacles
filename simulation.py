"""
Dumb simulation of how likely some particular draw scenarios are.
"""
from collections import defaultdict
import random


class Card:
    "An arkham horror card."
    def __init__(self, name):
        assert isinstance(name, str), 'Must provide str name'
        self.name = name

    def __hash__(self):
        return hash(('card', self.name))

    def __eq__(self, other):
        try:
            return self.name == other.name
        except AttributeError:
            return False


class Deck:
    @classmethod
    def from_cards(cls, cards, size=33):
        """
        Returns a deck of cards.

        Provide the mandatory cards, and filler cards will be added.
        """
        if len(cards) > size:
            n = len(cards)
            raise ValueError(("You provided {n} cards but specified a deck "
                              "size of {size}").format(n=n, size=size))

        cards = list(cards)
        # Add filler cards
        cards.extend(filler_cards(n=size - len(cards)))

        # shuffle the deck
        random.shuffle(cards)

        return cls(cards)

    def __init__(self, cards):
        self.cards = cards
        self.shuffle()

    def shuffle(self, cards=()):
        self.cards.extend(cards)
        random.shuffle(self.cards)

    def draw(self, n=1):
        res = [self.cards.pop() for _ in range(n)]
        return res


class Noteworthy:
    """
    Noteworthy cards.

    Use convenience name 'N'
    """
    Shrivelling = Card('Shrivelling')
    SongOfTheDead = Card('Song of the Dead')
    ArcaneInitiate = Card('Arcane Initiate')
    EmergencyCache = Card('Emergency Cache')
    SpiritAthame = Card('That dagger')
N = Noteworthy


def filler_cards(n):
    "Return filler cards."
    return [Card('Filler') for _ in range(n)]


class StrategyExecutor:
    """
    Evaluates the proposed strategies.

    Supports simulations of draw, mulligan, and post-mulligan play.
    """
    def execute_strategy(self, strategy):
        """
        Runs the simulation.
        """
        deck = strategy.deck()

        opening_draw = deck.draw(strategy.starting_hand_size)

        mulligan = strategy.mulligan(opening_draw)

        if mulligan:
            hand = [X for X in opening_draw if X not in mulligan]
            # Set cards aside, draw up hand, then shuffle set aside cards into deck
            hand.extend(deck.draw(len(mulligan)))
            deck.shuffle(mulligan)
        else:
            hand = opening_draw

        # Return result
        return strategy.simulate(hand, deck) or "Crap"

    def simulate_all_strategies(self, n_iterations=5000):
        strategies = Strategy.subclasses
        counter = defaultdict(int)
        for strategy_class in strategies:

            strat = strategy_class()
            print("Simulating {}".format(strat))

            for _ in range(n_iterations):
                res = self.execute_strategy(strat)
                counter[res] += 1

            self.print_results(counter)
            # prep for next run
            counter.clear()

    def print_results(self, counter):
        # TODO: handle ordering results from bestest to worstest
        summary = sorted(counter.items())
        iterations = sum(counter.values())

        sum_pct = 0
        print("Results" + '(n={})'.format(iterations))
        for title, count in summary:
            pct = 100.0 * count / iterations
            sum_pct += pct
            print("{title:<30}\t{count:5d} ({pct:5.02f}%)".format(title=title,
                                                                  count=count,
                                                                  pct=pct))
        # print("Total" + " " * 33 + "{sum_pct:5.02f}%".format(sum_pct=sum_pct))


class StratMeta(type):
    """
    Automatically registers any Strat subclasses
    """
    def __init__(cls, name, bases, dict):
        # First run (for Strat)
        if not hasattr(cls, 'subclasses'):
            cls.subclasses = []
        else:  # all subsequent runs (Strat subclasses)
            cls.subclasses.append(cls)
        super(StratMeta, cls).__init__(name, bases, dict)


class Strategy:
    """
    Base class for strategies.

    Subclass this and customize behavior.
    """
    __metaclass__ = StratMeta

    # customize this for e.g., painter
    starting_hand_size = 5

    def deck(self):
        """
        Return a fresh deck.
        """
        raise NotImplementedError()

    def mulligan(self, cards):
        "Return a list of cards you want to mulligan."
        raise NotImplementedError()

    def simulate(self, hand, deck):
        """
        Post-mulligan hand. Do whatever.

        If your strategy works return a str describing what you got.

        If you return nothing you'll get the default value of "Crap".
        """
        raise NotImplementedError()

    def __str__(self):
        return "Strategy: " + self.__class__.__name__

    def __repr__(self):
        return str(self)


class Akiliu(Strategy):
    def deck(self):
        # Add the cards we care about
        cards = [
            N.Shrivelling,
            N.Shrivelling,
            N.SongOfTheDead,
            N.ArcaneInitiate,
            N.ArcaneInitiate,
            N.EmergencyCache,
            N.EmergencyCache,
            N.SpiritAthame,
        ]
        return Deck.from_cards(cards, size=33)

    def mulligan(self, cards):
        return [X for X in cards
                if X not in {N.Shrivelling, N.ArcaneInitiate}]

    def simulate(self, hand, deck):
        if N.Shrivelling in hand and N.ArcaneInitiate in hand:
            return 'Ideal'

        if N.Shrivelling in hand:
            return 'Shrivelling'

        if N.ArcaneInitiate in hand:
            # use ally to find shrivelling
            peek = deck.draw(3)
            if N.Shrivelling in peek:
                return 'Ideal w/ Ally'
            if N.SongOfTheDead in peek:
                return 'SongOfTheDead w/ Ally'
            if N.EmergencyCache in peek:
                return 'Ally + Emergency Cache'

            # pretend you see no spells. Just draw twice
            deck.shuffle(peek)
            drawn = deck.draw(2)
            if N.Shrivelling in drawn:
                return 'Ideal w/ Ally + Draw'
            if N.SongOfTheDead in drawn:
                return 'SongOfTheDead w/ Ally + Draw'
            if N.EmergencyCache in peek:
                return 'Ally + Emergency Cache w/ Draw'
        else:
            # Ignore semantics of playing EC if it's in hand - you get the same # of cards
            hand.extend(deck.draw(3))
            if N.Shrivelling in hand:
                return 'Ideal w/ Ally + dead draws'

        if N.SongOfTheDead in hand:
            return 'SongOfTheDead'

        if N.EmergencyCache in hand:
            return 'Ally + Emergency Cache'


if __name__ == '__main__':
    se = StrategyExecutor()
    se.simulate_all_strategies(50000)
